require "spec"
require "./spec-helper.cr"


describe "Item" do 
  describe "new"  do 
    
    it "creates with value" do 
      item = Item.new id: 0, val: 100, wgt: 1_000, city: 2

      item.id.should   eq (0)
      item.val.should  eq (100)
      item.wgt.should  eq (1_000)
      item.city.should eq (2)
    end 

  end 

  describe "clone" do 
    it "creates a deep copy of self" do 
      original = Item.new(0,0,0,0)
      clone = original.clone 
      
      clone.id.should   eq original.id
      clone.val.should  eq original.val
      clone.wgt.should  eq original.wgt
      clone.city.should eq original.city
    end 
  end 

end 
