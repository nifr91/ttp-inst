# Ricardo Nieto Fuentes
# nifr91@gmail.com

require "./item"
require "./city"
require "./thief" 

class TTP 
  getter name      : String
  getter kp_type   : String
  getter ncities   : Int32
  getter nitems    : Int32
  getter mx_wgt    : Int32
  getter mx_vel    : Float64
  getter mn_vel    : Float64
  getter rratio    : Float64
  getter edge_type : String
  getter cities    : Array(City)
  getter items     : Array(Item)
  getter nu        : Float64

 def initialize(
    @name      ,
    @kp_type   ,
    @ncities   ,
    @nitems    ,
    @mx_wgt    ,
    @mx_vel    ,
    @mn_vel    ,
    @rratio    ,
    @edge_type ,
    @cities    ,
    @items     )

    @nu = (@mx_vel - @mn_vel) / @mx_wgt 
 end 
end 

require "./ttp/*"
