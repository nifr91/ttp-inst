# Ricardo Nieto Fuentes
# nifr91@gmail.com

class TTP 
  
  # Lee el archivo de instancia  y regresa el objeto que la representa.
  #
  # El formato del archivo esperado es  :
  #
  # ```text
  #   ________________________________________________________________
  #  | PROBLEM_NAME:           a280-TTP                               |
  #  | KNAPSACK_DATA_TYPE:     uncorrelated                           |
  #  | DIMENSION:              280                                    |
  #  | NUMBER_OF_ITEMS:        279                                    |
  #  | CAPACITY_OF_KNAPSACK:   25437                                  |
  #  | MIN_SPEED:              0.1                                    |
  #  | MAX_SPEED:              1                                      |
  #  | RENTING_RATIO:          7.76                                   |
  #  | EDGE_WEIGHT_TYPE:       CEIL_2D                                |
  #  | NODE_COORD_SECTION (INDEX, X, Y):                              |
  #  | 1 100 200                                                      |
  #  | ...                                                            |
  #  | ITEMS_SECTION (INDEX, PROFIT, WEIGHT, ASSIGNED_NODE_NUMBER):   |
  #  | 1 788 971 2                                                    |
  #  | ...                                                            |
  #  |________________________________________________________________|
  # ```
  #
  def self.load(filename : String) 
   
    # Leer el archivo 
    io = File.new(filename, "r")

    # Leer las primeras líneas 
    name      = io.read_line.split(":")[-1].strip
    kp_type   = io.read_line.split(":")[-1].strip
    ncities   = io.read_line.split(":")[-1].strip.to_f.to_i
    nitems    = io.read_line.split(":")[-1].strip.to_f.to_i
    mx_wgt    = io.read_line.split(":")[-1].strip.to_f.to_i
    mn_vel    = io.read_line.split(":")[-1].strip.to_f
    mx_vel    = io.read_line.split(":")[-1].strip.to_f
    rratio    = io.read_line.split(":")[-1].strip.to_f
    edge_type = io.read_line.split(":")[-1].strip

    # Linea en blanco (describe el formato)
    io.read_line

    # Leer las ciudades 
    cities = Array.new(ncities) do 
      vals = io.read_line.split(/\s+/).map(&.to_f)
      City.new(id: vals[0].to_i - 1, x: vals[1], y: vals[2])
    end

    # Linea en blanco (describe el formato)
    io.read_line 

    # Leer los objetos 
    items = Array.new(nitems) do 
      vals = io.read_line.split(/\s+/).map(&.to_f.to_i)
      Item.new(id: vals[0] - 1,val: vals[1],wgt: vals[2],city: vals[3] - 1)
    end
    
    TTP.new(
      name:      name,
      kp_type:   kp_type,
      ncities:   ncities,
      nitems:    nitems,
      mx_wgt:    mx_wgt,
      mx_vel:    mx_vel,
      mn_vel:    mn_vel,
      rratio:    rratio,
      edge_type: edge_type,
      cities:    cities,
      items:     items) 
  end 
end 
